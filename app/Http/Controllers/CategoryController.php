<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    function list(){
        $data_category = Category::all();
        return view ("category-list")
        ->with("data_category", $data_category);
    }
    function create(){
        return view("category-create");
    }
    function save(Request $request){
        $data_category=Category::create([
            "nama_category" =>$request-> input("nama_category"),
            "slug" =>$request->input ("slug"),
            "urutan" =>$request->input ("urutan"),
            "status" =>$request->input ("status")
        ]);
        if($data_category){
            return redirect(url("category"))
            ->with("status","berhasil");
        }else{
            return redirect(url("category"))
            ->with("status","gagal");
        }
    }

    function edit($id){
        $data_category = Category::find($id);
        return view ("category-edit")
        ->with("data_category", $data_category);
    }

    function update($id, Request $request){
        $data_category = Category::find($id);
        $data_category->nama_category = $request->input("nama_category");
        $data_category->slug = $request->input("slug");
        $data_category->urutan = $request->input("urutan");
        $data_category->status = $request->input("status");
    {
        $data_category->save();
        return redirect(url("category"));
    }
    }
    function delete($id){
        $data_category = Category::find($id);
        $data_category-> delete();
        return redirect(url("category"));
}
}

